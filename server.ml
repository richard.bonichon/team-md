open Base

let respond ?(status = `OK) ~content_type body =
  Cohttp_lwt_unix.Server.respond_string
    ~headers: (Cohttp.Header.of_list [ "Content-Type", content_type ])
    ~status
    ~body
    ()

let respond_text = respond ~content_type: "text/plain; charset=utf-8"
let respond_json = respond ~content_type: "application/json; charset=utf-8"

let not_found () =
  respond_text ~status: `Not_found "not found"

let diff_rex = rex "/diff/(\\d{4})-(\\d{2})-(\\d{2})"

let serve (get_state: unit -> State.t) name_map _connection
    (request: Cohttp.Request.t) _request_body =
  match request.meth with
    | `GET ->
        let uri = Uri.of_string request.resource in
        let path = Uri.path uri in
        if path = "/state" then
          respond_json (JSON.encode_u (State.encode (get_state ())))
        else (
          match path =~*** diff_rex with
            | Some (year, month, day) ->
                let year = int_of_string year in
                let month = int_of_string month in
                let day = int_of_string day in
                let from = State.Day.{ year; month; day } in
                respond_text (Markdown.make_timeline ~from (get_state ()) name_map)
            | None ->
                not_found ()
        )
    | _ ->
        not_found ()
