# The Team Markdown Preparator That Lists Too Many Issues

Clone the repository and install through opam:
```
git clone https://gitlab.com/romain.nl/team-md
opam install team-md
```
Then run from anywhere with:
```
team-md --help
```
This will show the command-line help. You'll want to pass a list of projects
and users to track, for instance:
```
team-md tezos/tezos Romain@romain.nl
```
This starts a daemon that will regularly poll GitLab's API to notice changes
to issues and merge requests that are authored by or assigned to tracked users.
It then provides an HTTP server to serve the following endpoints:
- http://localhost:8000/diff/YYYY-MM-DD — a markdown that shows what happened since YYYY-MM-DD;
- http://localhost:8000/state — the internal state of the daemon.
